﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControl_ThemNguoiDung
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbx_User = New System.Windows.Forms.TextBox()
        Me.tbx_Hoten = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_Huy = New System.Windows.Forms.Button()
        Me.btn_ThemUser = New System.Windows.Forms.Button()
        Me.tbx_Pass = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dt_Ngaysinh = New System.Windows.Forms.DateTimePicker()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 115)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Ngày sinh"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Họ và tên:"
        '
        'tbx_User
        '
        Me.tbx_User.Location = New System.Drawing.Point(113, 61)
        Me.tbx_User.Name = "tbx_User"
        Me.tbx_User.Size = New System.Drawing.Size(156, 20)
        Me.tbx_User.TabIndex = 5
        '
        'tbx_Hoten
        '
        Me.tbx_Hoten.Location = New System.Drawing.Point(113, 87)
        Me.tbx_Hoten.Name = "tbx_Hoten"
        Me.tbx_Hoten.Size = New System.Drawing.Size(156, 20)
        Me.tbx_Hoten.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.DarkTurquoise
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(313, 38)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "THÊM NGƯỜI DÙNG"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_Huy
        '
        Me.btn_Huy.Location = New System.Drawing.Point(194, 179)
        Me.btn_Huy.Name = "btn_Huy"
        Me.btn_Huy.Size = New System.Drawing.Size(75, 23)
        Me.btn_Huy.TabIndex = 9
        Me.btn_Huy.Text = "Hủy"
        Me.btn_Huy.UseVisualStyleBackColor = True
        '
        'btn_ThemUser
        '
        Me.btn_ThemUser.Location = New System.Drawing.Point(113, 179)
        Me.btn_ThemUser.Name = "btn_ThemUser"
        Me.btn_ThemUser.Size = New System.Drawing.Size(75, 23)
        Me.btn_ThemUser.TabIndex = 7
        Me.btn_ThemUser.Text = "Thêm mới"
        Me.btn_ThemUser.UseVisualStyleBackColor = True
        '
        'tbx_Pass
        '
        Me.tbx_Pass.Location = New System.Drawing.Point(113, 138)
        Me.tbx_Pass.Name = "tbx_Pass"
        Me.tbx_Pass.Size = New System.Drawing.Size(156, 20)
        Me.tbx_Pass.TabIndex = 5
        Me.tbx_Pass.UseSystemPasswordChar = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 141)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Mật khẩu"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(23, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(84, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Tên đăng nhập:"
        '
        'dt_Ngaysinh
        '
        Me.dt_Ngaysinh.Location = New System.Drawing.Point(113, 112)
        Me.dt_Ngaysinh.Name = "dt_Ngaysinh"
        Me.dt_Ngaysinh.Size = New System.Drawing.Size(156, 20)
        Me.dt_Ngaysinh.TabIndex = 11
        '
        'UserControl_QuanLyDangNhap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.dt_Ngaysinh)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbx_Pass)
        Me.Controls.Add(Me.tbx_User)
        Me.Controls.Add(Me.tbx_Hoten)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_Huy)
        Me.Controls.Add(Me.btn_ThemUser)
        Me.Name = "UserControl_QuanLyDangNhap"
        Me.Size = New System.Drawing.Size(313, 229)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbx_User As System.Windows.Forms.TextBox
    Friend WithEvents tbx_Hoten As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_Huy As System.Windows.Forms.Button
    Friend WithEvents btn_ThemUser As System.Windows.Forms.Button
    Friend WithEvents tbx_Pass As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dt_Ngaysinh As System.Windows.Forms.DateTimePicker

End Class
