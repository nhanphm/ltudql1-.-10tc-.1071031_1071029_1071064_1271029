﻿Public Class Form_QuanLyNguoiDung
    Dim dsUser As DataTable
    Dim idSelect As Integer

    Private Sub Form_QuanLyNguoiDung_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        SetupDataGridView()

        Dim truyvan As String = "Select MaUser, Username, Password, QuyenHan, Deleted From [dbo].[User]"
        dsUser = CSDL.Doc(truyvan)

        PopulateDataGridView()

        btn_them.Enabled = False
        btn_doipass.Enabled = False

    End Sub

    Private Sub SetupDataGridView()

        DataGridView1.ColumnCount = 5
        ' Định dạng tiêu đề cột        
        With DataGridView1.ColumnHeadersDefaultCellStyle

            .BackColor = Color.Navy
            .BackColor = Color.Navy
            .ForeColor = Color.White
            .Font = New Font(DataGridView1.Font, FontStyle.Bold)

        End With

        With DataGridView1

            .GridColor = Color.Black
            .BackgroundColor = Color.AliceBlue
            .ForeColor = Color.Green
            .AlternatingRowsDefaultCellStyle.ForeColor = Color.Purple
            .Columns(0).Name = "Mã User"
            .Columns(1).Name = "Username"
            .Columns(2).Name = "Password"
            .Columns(3).Name = "Quyền hạn"
            .Columns(4).Name = "Deleted"
            .Columns(4).DefaultCellStyle.Font = New Font(Me.DataGridView1.DefaultCellStyle.Font, FontStyle.Italic)
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .MultiSelect = False
            .Dock = DockStyle.Fill
            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False

        End With

    End Sub

    Private Sub PopulateDataGridView()

        DataGridView1.Rows.Clear()
        For Each r As DataRow In dsUser.Rows
            DataGridView1.Rows.Add({r(0), r(1), r(2), r(3), r(4)})
        Next
    End Sub

    Private Sub DataGridView1_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DataGridView1.CellFormatting
        If Me.DataGridView1.Columns(e.ColumnIndex).Name = _
            "Deleted" Then

            If e.Value = 0 Then
                e.Value = "Đang hoạt động"
            Else
                e.Value = "Đã xóa"
                DataGridView1.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Red

            End If
            e.FormattingApplied = True


        End If
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged

        If DataGridView1.SelectedRows.Count > 0 Then
            Dim r As DataGridViewRow = DataGridView1.SelectedRows(0)

            idSelect = r.Cells(0).Value
            txt_username.Text = r.Cells(1).Value
            txt_password.Text = r.Cells(2).Value

            If r.Cells(4).Value = 0 Then
                btn_xoa.Text = "Xóa người dùng"
            Else
                btn_xoa.Text = "Kích hoạt lại"
            End If
        Else
            idSelect = -1
            txt_username.Text = ""
            txt_password.Text = ""
        End If

        btn_them.Enabled = False
        btn_doipass.Enabled = False
    End Sub

    Private Sub txt_username_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_username.TextChanged
        If txt_username.Text <> "" And txt_password.Text <> "" Then
            btn_them.Enabled = True
            btn_doipass.Enabled = False
        End If
    End Sub

    Private Sub btn_exit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_exit.Click
        Me.Close()
    End Sub

    Private Sub txt_password_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_password.TextChanged
        If txt_username.Text <> "" And txt_password.Text <> "" And idSelect > 0 Then
            btn_doipass.Enabled = True
        End If
    End Sub

    Private Sub btn_them_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_them.Click

        'Dim truyvan As String = "insert into [dbo].[User] values(,'" & txt_username.Text & "','" & txt_password.Text & "',1,0"
        Dim r As DataRow = dsUser.NewRow
        r(1) = txt_username.Text
        r(2) = txt_password.Text
        r(3) = 1
        r(4) = 0
        dsUser.Rows.Add(r)

        Try
            CSDL.Ghi(dsUser, "[dbo].[User]")
            PopulateDataGridView()
        Catch ex As Exception
            MessageBox.Show("Error!" & ex.Message)
        End Try


    End Sub

    Private Sub btn_doipass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_doipass.Click
        Dim truyvan As String
        Try
            truyvan = "Update [dbo].[User] Set password='" + txt_password.Text + "' Where Username ='" + txt_username.Text + "'"
            CSDL.Thuc_hien_lenh(truyvan)
        Catch ex As Exception
            MessageBox.Show("Error!" & ex.Message)
        End Try
        

        truyvan = "Select MaUser, Username, Password, QuyenHan, Deleted From [dbo].[User]"
        dsUser = CSDL.Doc(truyvan)

        PopulateDataGridView()

    End Sub

    Private Sub btn_xoa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_xoa.Click
        Dim truyvan As String
        If btn_xoa.Text = "Khóa người dùng" Then
            truyvan = "Update [dbo].[User] Set Deleted= 1 Where QuyenHan=1 and MaUser =" + idSelect.ToString
        Else
            truyvan = "Update [dbo].[User] Set Deleted= 0 Where QuyenHan=1 and MaUser =" + idSelect.ToString
        End If

        Try
            CSDL.Thuc_hien_lenh(truyvan)
        Catch ex As Exception
            MessageBox.Show("Error!" & ex.Message)
        End Try


        truyvan = "Select MaUser, Username, Password, QuyenHan, Deleted From [dbo].[User]"
        dsUser = CSDL.Doc(truyvan)

        PopulateDataGridView()
    End Sub
End Class