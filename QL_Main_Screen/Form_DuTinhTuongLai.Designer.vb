﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_DuTinhTuongLai
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSoTien = New System.Windows.Forms.TextBox()
        Me.txtSoTienDu = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtThoiGian = New System.Windows.Forms.TextBox()
        Me.btTinhToan = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(193, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nhập vào số tiền mong muốn đạt được"
        '
        'txtSoTien
        '
        Me.txtSoTien.Location = New System.Drawing.Point(16, 29)
        Me.txtSoTien.Name = "txtSoTien"
        Me.txtSoTien.Size = New System.Drawing.Size(100, 20)
        Me.txtSoTien.TabIndex = 1
        '
        'txtSoTienDu
        '
        Me.txtSoTienDu.Enabled = False
        Me.txtSoTienDu.Location = New System.Drawing.Point(16, 72)
        Me.txtSoTienDu.Name = "txtSoTienDu"
        Me.txtSoTienDu.Size = New System.Drawing.Size(100, 20)
        Me.txtSoTienDu.TabIndex = 2
        Me.txtSoTienDu.Text = "2000"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(183, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Số tiền dư để dành được hàng tháng"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 99)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(193, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Thời gian dự định sẽ đạt được ( tháng )"
        '
        'txtThoiGian
        '
        Me.txtThoiGian.Location = New System.Drawing.Point(16, 115)
        Me.txtThoiGian.Name = "txtThoiGian"
        Me.txtThoiGian.Size = New System.Drawing.Size(100, 20)
        Me.txtThoiGian.TabIndex = 5
        '
        'btTinhToan
        '
        Me.btTinhToan.Location = New System.Drawing.Point(16, 160)
        Me.btTinhToan.Name = "btTinhToan"
        Me.btTinhToan.Size = New System.Drawing.Size(75, 23)
        Me.btTinhToan.TabIndex = 6
        Me.btTinhToan.Text = "Tính toán"
        Me.btTinhToan.UseVisualStyleBackColor = True
        '
        'Form_DuTinhTuongLai
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(222, 201)
        Me.Controls.Add(Me.btTinhToan)
        Me.Controls.Add(Me.txtThoiGian)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtSoTienDu)
        Me.Controls.Add(Me.txtSoTien)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form_DuTinhTuongLai"
        Me.Text = "Dự Tính Tương Lai"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSoTien As System.Windows.Forms.TextBox
    Friend WithEvents txtSoTienDu As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtThoiGian As System.Windows.Forms.TextBox
    Friend WithEvents btTinhToan As System.Windows.Forms.Button
End Class
