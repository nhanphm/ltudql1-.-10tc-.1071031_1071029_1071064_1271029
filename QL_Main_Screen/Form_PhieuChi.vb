﻿Public Class Form_PhieuChi
    Dim dsPC As DataTable
    Dim loaichi As DataTable
    Dim nguoichi As DataTable
    Dim taikhoan As DataTable

    Private Sub SetupDataGridView()

        DataGridView1.ColumnCount = 7
        ' Định dạng tiêu đề cột        
        With DataGridView1.ColumnHeadersDefaultCellStyle

            .BackColor = Color.Navy
            .BackColor = Color.Navy
            .ForeColor = Color.White
            .Font = New Font(DataGridView1.Font, FontStyle.Bold)

        End With

        With DataGridView1

            .GridColor = Color.Black
            .BackgroundColor = Color.AliceBlue
            .ForeColor = Color.Green
            .AlternatingRowsDefaultCellStyle.ForeColor = Color.Purple
            .Columns(0).Name = "STT"
            .Columns(0).Width = 40

            .Columns(1).Name = "Ngày tháng"
            '.Columns(1).Width = 200
            .Columns(2).Name = "Loại chi"
            .Columns(3).Name = "Số tiền"
            .Columns(4).Name = "Tài khoản"
            .Columns(5).Name = "Người chi"
            .Columns(6).Name = "Chi tiết"
            .Columns(6).Width = 400
            .Columns(5).DefaultCellStyle.Font = New Font(Me.DataGridView1.DefaultCellStyle.Font, FontStyle.Italic)
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .MultiSelect = False
            '.Dock = DockStyle.Fill
            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False

        End With

    End Sub

    Private Sub Form_PhieuChi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        SetupDataGridView()
        load_loaiChi()
        load_taiKhoan()
        load_nguoiChi()
        dsPC = CSDL.Doc_cau_truc("[dbo].[PhieuChi] Where MaUserTao = " & My.Settings.MaUser.ToString & " Order by Ngay DESC")

        cb_hienthi.SelectedIndex = 0
        dt_ngaythang.Value = Date.Now.Date


    End Sub


    Private Sub load_loaiChi()

        loaichi = CSDL.Doc_cau_truc("[dbo].[ChiPhi]")

        cb_loaichi.DataSource = loaiChi
        cb_loaichi.DisplayMember = "Ten"
        cb_loaichi.ValueMember = "MaChiPhi"

        Dim loaiChi_filter As DataTable = loaiChi.Copy()

        loaiChi_filter.Rows.Add({-1, "..."})
        cb_loaichi_filter.DataSource = loaiChi_filter
        cb_loaichi_filter.DisplayMember = "Ten"
        cb_loaichi_filter.ValueMember = "MaChiPhi"
        cb_loaichi_filter.SelectedValue = -1

    End Sub

    Private Sub load_taiKhoan()
        taikhoan = CSDL.Doc_cau_truc("[dbo].[NganHang] where TinhTrang = 0 and MaUser = " & My.Settings.MaUser.ToString)

        cb_taikhoan.DataSource = taiKhoan
        cb_taikhoan.DisplayMember = "Ten"
        cb_taikhoan.ValueMember = "MaNganHang"

        Dim taikhoan_filter As DataTable = taiKhoan.Copy()

        taikhoan_filter.Rows.Add({-1, "...", 0, 1, 0})
        cb_taikhoan_filter.DataSource = taikhoan_filter
        cb_taikhoan_filter.DisplayMember = "Ten"
        cb_taikhoan_filter.ValueMember = "MaNganHang"
        cb_taikhoan_filter.SelectedValue = -1
    End Sub

    Private Sub load_nguoiChi()
        nguoichi = CSDL.Doc_cau_truc("[dbo].[User] where Deleted = 0 ")

        cb_nguoichi.DataSource = nguoiChi
        cb_nguoichi.DisplayMember = "Username"
        cb_nguoichi.ValueMember = "MaUser"

        cb_nguoichi.SelectedValue = My.Settings.MaUser

        Dim nguoichi_filter As DataTable = nguoiChi.Copy()

        nguoichi_filter.Rows.Add({-1, "...", "1", 1, 0})
        cb_nguoichi_filter.DataSource = nguoichi_filter
        cb_nguoichi_filter.DisplayMember = "Username"
        cb_nguoichi_filter.ValueMember = "MaUser"

        cb_nguoichi_filter.SelectedValue = -1

    End Sub

    Private Sub cb_taikhoan_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_taikhoan.SelectedIndexChanged

        Dim r As DataRowView = cb_taikhoan.SelectedItem
        Dim money As Decimal
        money = Val(r(2))
        lb_sotien.Text = money.ToString("#,## VNĐ")

    End Sub

    Private Sub PopulateDataGridView(ByVal dt() As DataRow)
        If dt.Count = 0 Then
            DataGridView1.Rows.Clear()
            Return
        End If
        Dim stt As Integer = 0
        DataGridView1.Rows.Clear()
        For Each r As DataRow In dt
            stt += 1
            DataGridView1.Rows.Add({stt, r(1), r(6), r(2), r(5), r(3), r(7)})
            With DataGridView1.Rows(stt - 1)
                .Tag = r(0)
            End With
        Next
    End Sub

    Private Sub cb_hienthi_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_hienthi.SelectedIndexChanged
        setData()
    End Sub

    Private Sub setData()
        If cb_hienthi.SelectedIndex = 1 Then
            PopulateDataGridView(dsPC.Select())
        Else

            Dim time As Date = DateSerial(Now.Year, Now.Month, 1)

            Dim dt() As DataRow = dsPC.Select("Ngay >=#" & time.ToString & "#", "Ngay DESC")
            PopulateDataGridView(dt)

        End If
    End Sub
    Private Sub txt_sotien_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sotien.TextChanged
        If txt_sotien.Text = "" Then
            Return
        End If

        Dim money As Decimal = txt_sotien.Text
        txt_sotien.Text = money.ToString("#,##")
        txt_sotien.Select(txt_sotien.Text.Length, 0)
    End Sub

    Private Sub txt_sotien_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_sotien.KeyPress
        If Not Char.IsDigit(e.KeyChar) And Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Function kiemtra_Nhap() As Decimal

        If txt_sotien.Text = "" Then
            MessageBox.Show("Vui lòng nhập số tiền!")
            Return 0
        End If

        Dim sotien As Decimal = txt_sotien.Text
        Dim r As DataRowView = cb_taikhoan.SelectedItem
        Dim sotienco As Decimal = Val(r(2))

        If sotien > sotienco Then
            MessageBox.Show("Số tiền hiện tại không đủ!")
            Return 0
        End If

        Return sotienco - sotien

    End Function

    Private Sub DataGridView1_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DataGridView1.CellFormatting

        If Me.DataGridView1.Columns(e.ColumnIndex).Name = _
            "Loại chi" Then
            e.Value = loaichi.Select("MaChiPhi = " & e.Value.ToString)(0)(1)
            e.FormattingApplied = True
        End If

        If Me.DataGridView1.Columns(e.ColumnIndex).Name = _
            "Tài khoản" Then

            e.Value = taikhoan.Select("MaNganHang = " & e.Value.ToString)(0)(1)
        End If

        If Me.DataGridView1.Columns(e.ColumnIndex).Name = _
            "Người chi" Then

            e.Value = nguoichi.Select("MaUser = " & e.Value.ToString)(0)(1)

        End If


        If Me.DataGridView1.Columns(e.ColumnIndex).Name = _
            "Số tiền" Then
            e.CellStyle.Format = "#,##"
            e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End If

    End Sub

    Private Sub btn_them_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_them.Click
        Dim sotienconlai As Decimal = kiemtra_Nhap()
        If sotienconlai > 0 Then

            Dim item As DataRow = dsPC.NewRow
            item(1) = dt_ngaythang.Value
            item(2) = txt_sotien.Text
            item(3) = cb_nguoichi.SelectedValue
            item(4) = My.Settings.MaUser
            item(5) = cb_taikhoan.SelectedValue
            item(6) = cb_loaichi.SelectedValue
            item(7) = txt_noidung.Text

            dsPC.Rows.Add(item)
            Try
                CSDL.Ghi(dsPC, "[dbo].[PhieuChi]")

                Dim truyvan As String = "Update [dbo].[NganHang] Set SoTien=" & sotienconlai.ToString & " Where MaNganHang ='" & cb_taikhoan.SelectedValue.ToString & "'"
                CSDL.Thuc_hien_lenh(truyvan)

                load_taiKhoan()
                setData()
                txt_noidung.Text = ""
                txt_sotien.Text = ""
            Catch ex As Exception
                MessageBox.Show("Error!" & ex.Message)
            End Try


        End If

    End Sub

    Private Sub btn_xoa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_xoa.Click
        If DataGridView1.SelectedRows.Count = 0 Then
            Return
        End If
        Dim idSelect As Integer = DataGridView1.SelectedRows(0).Tag
        
        If MessageBox.Show("Bạn muốn xóa tài khoản này?", "Cảnh báo!", MessageBoxButtons.YesNo) = vbNo Then
            Return
        End If
        Dim truyvan As String
        Try
            truyvan = "Delete From [dbo].[PhieuChi] Where MaPhieuChi=" + idSelect.ToString
            CSDL.Thuc_hien_lenh(truyvan)

            dsPC.Rows.RemoveAt(DataGridView1.SelectedRows(0).Index)

            setData()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class