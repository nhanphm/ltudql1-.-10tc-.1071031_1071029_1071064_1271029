﻿Public Class Form_QuanLyTaiKhoan
    Dim dsTK As DataTable
    Dim idSelect As Integer

    Private Sub Form_QuanLyTaiKhoan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.CenterScreen
        SetupDataGridView()

        Dim truyvan As String = "Select MaNganHang,Ten, SoTien,MaUser, TinhTrang From [dbo].[NganHang] where MaUser=" & My.Settings.MaUser.ToString
        dsTK = CSDL.Doc(truyvan)

        PopulateDataGridView()

        btn_themTK.Enabled = False
    End Sub

    Private Sub SetupDataGridView()

        DataGridView1.ColumnCount = 4
        ' Định dạng tiêu đề cột        
        With DataGridView1.ColumnHeadersDefaultCellStyle

            .BackColor = Color.Navy
            .BackColor = Color.Navy
            .ForeColor = Color.White
            .Font = New Font(DataGridView1.Font, FontStyle.Bold)

        End With

        With DataGridView1

            .GridColor = Color.Black
            .BackgroundColor = Color.AliceBlue
            .ForeColor = Color.Green
            .AlternatingRowsDefaultCellStyle.ForeColor = Color.Purple
            .Columns(0).Name = "STT"
            .Columns(0).Width = 40

            .Columns(1).Name = "Tên Tài khoản"
            .Columns(1).Width = 200
            .Columns(2).Name = "Số dư"
            .Columns(3).Name = "Tình trạng"
            .Columns(3).DefaultCellStyle.Font = New Font(Me.DataGridView1.DefaultCellStyle.Font, FontStyle.Italic)
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .MultiSelect = False
            .Dock = DockStyle.Fill
            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False

        End With

    End Sub

    Private Sub PopulateDataGridView()
        Dim at As Integer
        Try
            at = DataGridView1.SelectedRows(0).Index
        Catch ex As Exception
            at = 0
        End Try

        Dim stt As Integer = 0
        DataGridView1.Rows.Clear()
        For Each r As DataRow In dsTK.Rows
            stt += 1
            DataGridView1.Rows.Add({stt, r(1), r(2), r(4)})
            With DataGridView1.Rows(stt - 1)
                .Tag = r(0)
            End With
        Next

        DataGridView1.Rows(at).Selected = True
    End Sub

    Private Sub DataGridView1_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles DataGridView1.CellFormatting
        If Me.DataGridView1.Columns(e.ColumnIndex).Name = _
            "Tình trạng" Then

            If e.Value = 0 Then
                e.Value = "Đang hoạt động"
            Else
                e.Value = "Đã khóa"
                DataGridView1.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Red

            End If
            e.FormattingApplied = True


        End If
        If Me.DataGridView1.Columns(e.ColumnIndex).Name = _
            "Số dư" Then
            e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End If

    End Sub

    Private Sub txt_tenTKmoi_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_tenTKmoi.TextChanged, txt_soDumoi.TextChanged

        If txt_tenTKmoi.Text <> "" And txt_soDumoi.Text <> "" Then

            btn_themTK.Enabled = True

        End If

    End Sub

    Private Sub btn_themTK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_themTK.Click
        Dim r As DataRow = dsTK.NewRow
        r(1) = txt_tenTKmoi.Text
        r(2) = txt_soDumoi.Text
        r(3) = My.Settings.MaUser
        r(4) = 0

        dsTK.Rows.Add(r)

        Try
            CSDL.Ghi(dsTK, "[dbo].[NganHang]")
            PopulateDataGridView()
            txt_tenTKmoi.Text = ""
            txt_soDumoi.Text = ""
        Catch ex As Exception
            MessageBox.Show("Error!" & ex.Message)
        End Try

    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged

        If DataGridView1.SelectedRows.Count > 0 Then
            btn_capnhat.Enabled = True
            btn_khoataikhoan.Enabled = True
            Dim r As DataGridViewRow = DataGridView1.SelectedRows(0)

            idSelect = r.Tag
            txt_tentaikhoan.Text = r.Cells(1).Value
            txt_sodu.Text = r.Cells(2).Value

            If r.Cells(3).Value = 0 Then
                btn_khoataikhoan.Text = "Khóa tài khoản"
            Else
                btn_khoataikhoan.Text = "Kích hoạt lại"
            End If
        Else
            idSelect = -1
            txt_tentaikhoan.Text = ""
            txt_sodu.Text = ""
            btn_capnhat.Enabled = False
            btn_khoataikhoan.Enabled = False
        End If

    End Sub

    Private Sub btn_capnhat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_capnhat.Click

        For Each r As DataRow In dsTK.Rows
            If idSelect = r(0) Then
                If r(1) = "Tiền mặt" Then
                    MessageBox.Show("Không thể xóa Tài khoản Tiền mặt!")
                    Return
                End If
                r(1) = txt_tentaikhoan.Text
                r(2) = txt_sodu.Text
                Exit For
            End If
        Next
        Try
            CSDL.Ghi(dsTK, "[dbo].[NganHang]")
            PopulateDataGridView()
        Catch ex As Exception
            MessageBox.Show("Error!" & ex.Message)
        End Try
    End Sub

    Private Sub btn_khoataikhoan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_khoataikhoan.Click

        For Each r As DataRow In dsTK.Rows
            If idSelect = r(0) Then
                If r(1) = "Tiền mặt" Then
                    MessageBox.Show("Không thể khóa Tài khoản Tiền mặt!")
                    Return
                End If
                If r(4) = 0 Then
                    r(4) = 1
                Else
                    r(4) = 0
                End If
                Exit For
            End If
        Next
        Try
            CSDL.Ghi(dsTK, "[dbo].[NganHang]")
            PopulateDataGridView()
        Catch ex As Exception
            MessageBox.Show("Error!" & ex.Message)
        End Try

    End Sub

    Private Sub btn_xoataikhoan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_xoataikhoan.Click
        idSelect = DataGridView1.SelectedRows(0).Tag
        If DataGridView1.SelectedRows(0).Cells(1).Value = "Tiền mặt" Then
            MessageBox.Show("Không thể xóa Tài khoản Tiền mặt!")
            Return
        End If
        If MessageBox.Show("Bạn muốn xóa tài khoản này?", "Cảnh báo!", MessageBoxButtons.YesNo) = vbNo Then
            Return
        End If
        Dim truyvan As String
        Try
            truyvan = "Delete From [dbo].[NganHang] Where MaNganHang=" + idSelect.ToString
            CSDL.Thuc_hien_lenh(truyvan)

            dsTK.Rows.RemoveAt(DataGridView1.SelectedRows(0).Index)

            PopulateDataGridView()
        Catch ex As Exception
            MessageBox.Show("Tài khoản đang được sử dụng! Không thể xóa")
        End Try


    End Sub

    Private Sub btn_thoat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_thoat.Click
        Me.Close()
    End Sub
End Class