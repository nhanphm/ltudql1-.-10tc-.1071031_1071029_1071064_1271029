﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_ThayDoiMatKhau
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.lb_User = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_laplai = New System.Windows.Forms.TextBox()
        Me.txt_matkhaumoi = New System.Windows.Forms.TextBox()
        Me.txt_matkhaucu = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_Huy = New System.Windows.Forms.Button()
        Me.btn_Thaydoi = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(128, 166)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(109, 17)
        Me.CheckBox1.TabIndex = 36
        Me.CheckBox1.Text = "Hiển thị mật khẩu"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'lb_User
        '
        Me.lb_User.AutoSize = True
        Me.lb_User.Location = New System.Drawing.Point(128, 64)
        Me.lb_User.Name = "lb_User"
        Me.lb_User.Size = New System.Drawing.Size(39, 13)
        Me.lb_User.TabIndex = 35
        Me.lb_User.Text = "Label6"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(38, 142)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 13)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "Nhập lại MK"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(38, 116)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 13)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Mật khẩu mới"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(38, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(84, 13)
        Me.Label5.TabIndex = 33
        Me.Label5.Text = "Tên đăng nhập:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(38, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Mật khẩu cũ"
        '
        'txt_laplai
        '
        Me.txt_laplai.Location = New System.Drawing.Point(128, 139)
        Me.txt_laplai.Name = "txt_laplai"
        Me.txt_laplai.Size = New System.Drawing.Size(156, 20)
        Me.txt_laplai.TabIndex = 26
        Me.txt_laplai.UseSystemPasswordChar = True
        '
        'txt_matkhaumoi
        '
        Me.txt_matkhaumoi.Location = New System.Drawing.Point(128, 113)
        Me.txt_matkhaumoi.Name = "txt_matkhaumoi"
        Me.txt_matkhaumoi.Size = New System.Drawing.Size(156, 20)
        Me.txt_matkhaumoi.TabIndex = 27
        Me.txt_matkhaumoi.UseSystemPasswordChar = True
        '
        'txt_matkhaucu
        '
        Me.txt_matkhaucu.Location = New System.Drawing.Point(128, 87)
        Me.txt_matkhaucu.Name = "txt_matkhaucu"
        Me.txt_matkhaucu.Size = New System.Drawing.Size(156, 20)
        Me.txt_matkhaucu.TabIndex = 25
        Me.txt_matkhaucu.UseSystemPasswordChar = True
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.DarkTurquoise
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(342, 38)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "QUẢN LÝ NGƯỜI DÙNG"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_Huy
        '
        Me.btn_Huy.Location = New System.Drawing.Point(209, 194)
        Me.btn_Huy.Name = "btn_Huy"
        Me.btn_Huy.Size = New System.Drawing.Size(75, 23)
        Me.btn_Huy.TabIndex = 32
        Me.btn_Huy.Text = "Hủy"
        Me.btn_Huy.UseVisualStyleBackColor = True
        '
        'btn_Thaydoi
        '
        Me.btn_Thaydoi.Location = New System.Drawing.Point(128, 194)
        Me.btn_Thaydoi.Name = "btn_Thaydoi"
        Me.btn_Thaydoi.Size = New System.Drawing.Size(75, 23)
        Me.btn_Thaydoi.TabIndex = 29
        Me.btn_Thaydoi.Text = "Thay đổi"
        Me.btn_Thaydoi.UseVisualStyleBackColor = True
        '
        'Form_ThayDoiMatKhau
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(342, 247)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.lb_User)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_laplai)
        Me.Controls.Add(Me.txt_matkhaumoi)
        Me.Controls.Add(Me.txt_matkhaucu)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_Huy)
        Me.Controls.Add(Me.btn_Thaydoi)
        Me.Name = "Form_ThayDoiMatKhau"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "THAY ĐỔI MẬT KHẨU"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents lb_User As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_laplai As System.Windows.Forms.TextBox
    Friend WithEvents txt_matkhaumoi As System.Windows.Forms.TextBox
    Friend WithEvents txt_matkhaucu As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btn_Huy As System.Windows.Forms.Button
    Friend WithEvents btn_Thaydoi As System.Windows.Forms.Button
End Class
