﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserControl_Login
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_Dangnhap = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_User = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_Pass = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btn_Huy = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btn_Dangnhap
        '
        Me.btn_Dangnhap.Location = New System.Drawing.Point(118, 106)
        Me.btn_Dangnhap.Name = "btn_Dangnhap"
        Me.btn_Dangnhap.Size = New System.Drawing.Size(75, 23)
        Me.btn_Dangnhap.TabIndex = 2
        Me.btn_Dangnhap.Text = "Đăng nhập"
        Me.btn_Dangnhap.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.DarkTurquoise
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(313, 38)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "ĐĂNG NHẬP"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txt_User
        '
        Me.txt_User.Location = New System.Drawing.Point(118, 55)
        Me.txt_User.Name = "txt_User"
        Me.txt_User.Size = New System.Drawing.Size(156, 20)
        Me.txt_User.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(28, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Tên đăng nhập:"
        '
        'txt_Pass
        '
        Me.txt_Pass.Location = New System.Drawing.Point(118, 80)
        Me.txt_Pass.Name = "txt_Pass"
        Me.txt_Pass.Size = New System.Drawing.Size(156, 20)
        Me.txt_Pass.TabIndex = 1
        Me.txt_Pass.UseSystemPasswordChar = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(28, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Mật khẩu"
        '
        'btn_Huy
        '
        Me.btn_Huy.Location = New System.Drawing.Point(199, 106)
        Me.btn_Huy.Name = "btn_Huy"
        Me.btn_Huy.Size = New System.Drawing.Size(75, 23)
        Me.btn_Huy.TabIndex = 3
        Me.btn_Huy.Text = "Hủy"
        Me.btn_Huy.UseVisualStyleBackColor = True
        '
        'UserControl_Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_Pass)
        Me.Controls.Add(Me.txt_User)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_Huy)
        Me.Controls.Add(Me.btn_Dangnhap)
        Me.Name = "UserControl_Login"
        Me.Size = New System.Drawing.Size(313, 150)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Dangnhap As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_User As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_Pass As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btn_Huy As System.Windows.Forms.Button

End Class
