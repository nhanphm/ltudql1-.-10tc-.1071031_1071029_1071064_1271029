﻿Public Class Form_ThayDoiMatKhau
    Dim dsUser As DataTable

    Private Sub Form_ThayDoiMatKhau_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lb_User.Text = My.Settings.Username
        CheckBox1.Checked = False

        txt_matkhaucu.UseSystemPasswordChar = True
        txt_matkhaumoi.UseSystemPasswordChar = True
        txt_laplai.UseSystemPasswordChar = True

        btn_Thaydoi.Enabled = False

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            txt_matkhaucu.UseSystemPasswordChar = False
            txt_matkhaumoi.UseSystemPasswordChar = False
            txt_laplai.UseSystemPasswordChar = False
        Else
            txt_matkhaucu.UseSystemPasswordChar = True
            txt_matkhaumoi.UseSystemPasswordChar = True
            txt_laplai.UseSystemPasswordChar = True
        End If
    End Sub

    Private Sub txt_laplai_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_laplai.TextChanged, txt_matkhaucu.TextChanged, txt_matkhaumoi.TextChanged

        If txt_laplai.Text <> "" And txt_matkhaucu.Text <> "" And txt_matkhaumoi.Text <> "" Then
            If txt_matkhaumoi.Text = txt_laplai.Text Then
                btn_Thaydoi.Enabled = True
            End If
        End If

    End Sub

    Private Sub btn_Huy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Huy.Click
        Me.Close()
    End Sub

    Private Sub btn_Thaydoi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Thaydoi.Click

        Dim truyvan As String = "Select MaUser From [dbo].[User] Where Username ='" + lb_User.Text + "' and Password = '" + txt_matkhaucu.Text + "'"

        dsUser = CSDL.Doc(truyvan)

        If dsUser.Rows.Count > 0 Then
            Try
                truyvan = "Update [dbo].[User] Set password='" + txt_matkhaumoi.Text + "' Where Username ='" + lb_User.Text + "'"
                CSDL.Thuc_hien_lenh(truyvan)
                MessageBox.Show("Thay đổi mật khẩu thành công")
            Catch ex As Exception
                MessageBox.Show("Error!" & ex.Message)
            End Try
        End If

    End Sub
End Class