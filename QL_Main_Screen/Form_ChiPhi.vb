﻿Public Class Form_ChiPhi
    Dim dsTK As DataTable
    Dim idSelect As Integer

    Private Sub Form_ChiPhi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.StartPosition = FormStartPosition.CenterParent

        SetupDataGridView()

        Dim truyvan As String = "Select MaChiPhi, Ten From [dbo].[ChiPhi]"
        dsTK = CSDL.Doc(truyvan)

        PopulateDataGridView()

        'btn_them.Enabled = False

    End Sub

    Private Sub SetupDataGridView()

        DataGridView1.ColumnCount = 2
        ' Định dạng tiêu đề cột        
        With DataGridView1.ColumnHeadersDefaultCellStyle

            .BackColor = Color.Navy
            .BackColor = Color.Navy
            .ForeColor = Color.White
            .Font = New Font(DataGridView1.Font, FontStyle.Bold)

        End With

        With DataGridView1

            .GridColor = Color.Black
            .BackgroundColor = Color.AliceBlue
            .ForeColor = Color.Green
            .AlternatingRowsDefaultCellStyle.ForeColor = Color.Purple
            .Columns(0).Name = "STT"
            .Columns(0).Width = 40

            .Columns(1).Name = "Loại Chi phí"
            .Columns(1).Width = 200
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .MultiSelect = False
            '.Dock = DockStyle.Fill
            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False

        End With

    End Sub

    Private Sub PopulateDataGridView()
        Dim at As Integer
        Try
            at = DataGridView1.SelectedRows(0).Index
        Catch ex As Exception
            at = 0
        End Try

        Dim stt As Integer = 0
        DataGridView1.Rows.Clear()
        For Each r As DataRow In dsTK.Rows
            stt += 1
            DataGridView1.Rows.Add({stt, r(1)})
            With DataGridView1.Rows(stt - 1)
                .Tag = r(0)
            End With
        Next

        'DataGridView1.Rows(at).Selected = True
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged

        If DataGridView1.SelectedRows.Count > 0 Then
            btn_sua.Enabled = True
            btn_xoa.Enabled = True
            Dim r As DataGridViewRow = DataGridView1.SelectedRows(0)

            idSelect = r.Tag
            txt_chiphi.Text = r.Cells(1).Value
        Else
            idSelect = -1
            txt_chiphi.Text = ""
            btn_sua.Enabled = False
            btn_xoa.Enabled = False
        End If

    End Sub

    Private Sub btn_them_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_them.Click
        Dim r As DataRow = dsTK.NewRow
        r(1) = txt_chiphi.Text

        dsTK.Rows.Add(r)

        Try
            CSDL.Ghi(dsTK, "[dbo].[ChiPhi]")
            PopulateDataGridView()
            txt_chiphi.Text = ""
        Catch ex As Exception
            MessageBox.Show("Error!" & ex.Message)
        End Try
    End Sub

    Private Sub btn_sua_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_sua.Click
        For Each r As DataRow In dsTK.Rows
            If idSelect = r(0) Then
                r(1) = txt_chiphi.Text
                Exit For
            End If
        Next
        Try
            CSDL.Ghi(dsTK, "[dbo].[ChiPhi]")
            PopulateDataGridView()
        Catch ex As Exception
            MessageBox.Show("Error!" & ex.Message)
        End Try
    End Sub

    Private Sub btn_xoa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_xoa.Click
        idSelect = DataGridView1.SelectedRows(0).Tag

        If MessageBox.Show("Bạn muốn xóa Loại chi phí này?", "Cảnh báo!", MessageBoxButtons.YesNo) = vbNo Then
            Return
        End If
        Dim truyvan As String
        Try
            truyvan = "Delete From [dbo].[ChiPhi] Where MaChiPhi=" + idSelect.ToString
            CSDL.Thuc_hien_lenh(truyvan)

            dsTK.Rows.RemoveAt(DataGridView1.SelectedRows(0).Index)

            PopulateDataGridView()
        Catch ex As Exception
            MessageBox.Show("Chi phí đang được sử dụng! Không thể xóa")
        End Try
    End Sub

    Private Sub btn_thoat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_thoat.Click
        Me.Close()
    End Sub
End Class