﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_QuanLyTaiKhoan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_thoat = New System.Windows.Forms.Button()
        Me.btn_capnhat = New System.Windows.Forms.Button()
        Me.btn_themTK = New System.Windows.Forms.Button()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btn_xoataikhoan = New System.Windows.Forms.Button()
        Me.txt_sodu = New System.Windows.Forms.TextBox()
        Me.btn_khoataikhoan = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_tentaikhoan = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_soDumoi = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_tenTKmoi = New System.Windows.Forms.TextBox()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.DarkTurquoise
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(856, 46)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "QUẢN LÝ TÀI KHOẢN"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_thoat
        '
        Me.btn_thoat.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_thoat.Location = New System.Drawing.Point(275, 175)
        Me.btn_thoat.Name = "btn_thoat"
        Me.btn_thoat.Size = New System.Drawing.Size(123, 41)
        Me.btn_thoat.TabIndex = 2
        Me.btn_thoat.Text = "Thoát"
        Me.btn_thoat.UseVisualStyleBackColor = True
        '
        'btn_capnhat
        '
        Me.btn_capnhat.Location = New System.Drawing.Point(11, 119)
        Me.btn_capnhat.Name = "btn_capnhat"
        Me.btn_capnhat.Size = New System.Drawing.Size(123, 41)
        Me.btn_capnhat.TabIndex = 2
        Me.btn_capnhat.Text = "Cập nhật tài khoản"
        Me.btn_capnhat.UseVisualStyleBackColor = True
        '
        'btn_themTK
        '
        Me.btn_themTK.Location = New System.Drawing.Point(273, 86)
        Me.btn_themTK.Name = "btn_themTK"
        Me.btn_themTK.Size = New System.Drawing.Size(123, 41)
        Me.btn_themTK.TabIndex = 2
        Me.btn_themTK.Text = "Thêm tài khoản"
        Me.btn_themTK.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 46)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.DataGridView1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.btn_thoat)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GroupBox2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Size = New System.Drawing.Size(856, 452)
        Me.SplitContainer1.SplitterDistance = 434
        Me.SplitContainer1.TabIndex = 0
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(434, 452)
        Me.DataGridView1.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btn_capnhat)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.btn_xoataikhoan)
        Me.GroupBox2.Controls.Add(Me.txt_sodu)
        Me.GroupBox2.Controls.Add(Me.btn_khoataikhoan)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txt_tentaikhoan)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(402, 166)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Thông tin tài khoản"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Số tiền hiện có:"
        '
        'btn_xoataikhoan
        '
        Me.btn_xoataikhoan.Location = New System.Drawing.Point(269, 119)
        Me.btn_xoataikhoan.Name = "btn_xoataikhoan"
        Me.btn_xoataikhoan.Size = New System.Drawing.Size(123, 41)
        Me.btn_xoataikhoan.TabIndex = 4
        Me.btn_xoataikhoan.Text = "Xóa tài khoản"
        Me.btn_xoataikhoan.UseVisualStyleBackColor = True
        '
        'txt_sodu
        '
        Me.txt_sodu.Location = New System.Drawing.Point(110, 55)
        Me.txt_sodu.Name = "txt_sodu"
        Me.txt_sodu.Size = New System.Drawing.Size(282, 20)
        Me.txt_sodu.TabIndex = 1
        '
        'btn_khoataikhoan
        '
        Me.btn_khoataikhoan.Location = New System.Drawing.Point(140, 119)
        Me.btn_khoataikhoan.Name = "btn_khoataikhoan"
        Me.btn_khoataikhoan.Size = New System.Drawing.Size(123, 41)
        Me.btn_khoataikhoan.TabIndex = 3
        Me.btn_khoataikhoan.Text = "Khóa tài khoản"
        Me.btn_khoataikhoan.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Tên tài khoản:"
        '
        'txt_tentaikhoan
        '
        Me.txt_tentaikhoan.Location = New System.Drawing.Point(110, 19)
        Me.txt_tentaikhoan.Name = "txt_tentaikhoan"
        Me.txt_tentaikhoan.Size = New System.Drawing.Size(282, 20)
        Me.txt_tentaikhoan.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_themTK)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txt_soDumoi)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txt_tenTKmoi)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 298)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(402, 143)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Thêm tài khoản"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(22, 63)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Số dư:"
        '
        'txt_soDumoi
        '
        Me.txt_soDumoi.Location = New System.Drawing.Point(114, 60)
        Me.txt_soDumoi.Name = "txt_soDumoi"
        Me.txt_soDumoi.Size = New System.Drawing.Size(282, 20)
        Me.txt_soDumoi.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tên tài khoản:"
        '
        'txt_tenTKmoi
        '
        Me.txt_tenTKmoi.Location = New System.Drawing.Point(114, 24)
        Me.txt_tenTKmoi.Name = "txt_tenTKmoi"
        Me.txt_tenTKmoi.Size = New System.Drawing.Size(282, 20)
        Me.txt_tenTKmoi.TabIndex = 0
        '
        'Form_QuanLyTaiKhoan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(856, 498)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form_QuanLyTaiKhoan"
        Me.Text = "QUẢN LÝ TÀI KHOẢN NGÂN HÀNG"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents btn_capnhat As System.Windows.Forms.Button
    Friend WithEvents btn_themTK As System.Windows.Forms.Button
    Friend WithEvents btn_thoat As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_tenTKmoi As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_soDumoi As System.Windows.Forms.TextBox
    Friend WithEvents btn_khoataikhoan As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents btn_xoataikhoan As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_sodu As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_tentaikhoan As System.Windows.Forms.TextBox
End Class
