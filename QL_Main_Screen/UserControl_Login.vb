﻿Public Class UserControl_Login

    Delegate Sub MsgLoginHandler(ByVal user As String, ByVal pass As String)
    Event LoginHandler As MsgLoginHandler

    Delegate Sub MsgLoginExitHandler()
    Event LoginExitHandler As MsgLoginExitHandler

    Private Sub btn_Dangnhap_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Dangnhap.Click

        If txt_User.Text <> "" And txt_Pass.Text <> "" Then

            RaiseEvent LoginHandler(txt_User.Text, txt_Pass.Text)

        End If

    End Sub

    Private Sub UserControl_Login_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        btn_Dangnhap.Enabled = False

    End Sub

    Private Sub txt_User_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_User.TextChanged, txt_Pass.TextChanged

        If txt_User.Text <> "" And txt_Pass.Text <> "" Then

            btn_Dangnhap.Enabled = True

        End If

    End Sub

    Private Sub btn_Huy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Huy.Click

        RaiseEvent LoginExitHandler()

    End Sub

    Private Sub txt_User_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txt_User.KeyDown, txt_Pass.KeyDown
        If e.KeyCode = Keys.Enter Then
            RaiseEvent LoginHandler(txt_User.Text, txt_Pass.Text)
        End If
    End Sub
End Class
