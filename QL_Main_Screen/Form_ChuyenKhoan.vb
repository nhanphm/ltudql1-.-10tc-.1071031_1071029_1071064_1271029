﻿Public Class Form_ChuyenKhoan
    Dim dsTK As DataTable
    Dim tk1 As String = ""
    Dim tk2 As String = ""

    Private Sub Form_ChuyenKhoan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.CenterScreen

        Dim truyvan As String = "Select MaNganHang,Ten, SoTien From [dbo].[NganHang] where TinhTrang=0 and MaUser=" & My.Settings.MaUser.ToString
        dsTK = CSDL.Doc(truyvan)

        If tk1 = "" Then
            tk1 = dsTK.Select("Ten='Tiền mặt'")(0)(0)
            tk2 = tk1
        End If
        SetupChuyenKhoan()


    End Sub

    Private Sub SetupChuyenKhoan()

        cb_tenTK1.DataSource = dsTK
        cb_tenTK1.DisplayMember = "Ten"
        cb_tenTK1.ValueMember = "MaNganHang"

        cb_tenTK2.DataSource = dsTK.Copy()
        cb_tenTK2.DisplayMember = "Ten"
        cb_tenTK2.ValueMember = "MaNganHang"

        cb_tenTK1.SelectedValue = tk1
        cb_tenTK2.SelectedValue = tk2


    End Sub

    Private Sub cb_tenTK1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_tenTK1.SelectedIndexChanged

        lb_tk1.Text = cb_tenTK1.Text
        Dim money As Decimal
        money = Val(dsTK.Rows(cb_tenTK1.SelectedIndex)(2))
        lb_sotien1.Text = money.ToString("#,## VNĐ")

    End Sub

    Private Sub cb_tenTK2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_tenTK2.SelectedIndexChanged

        lb_tk2.Text = cb_tenTK2.Text
        Dim money As Decimal
        money = Val(dsTK.Rows(cb_tenTK2.SelectedIndex)(2))
        lb_sotien2.Text = money.ToString("#,## VNĐ")
    End Sub


    Private Sub txt_soTien_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_soTien.TextChanged

        If txt_soTien.Text = "" Then
            Return
        End If

        Dim money As Decimal = txt_soTien.Text
        txt_soTien.Text = money.ToString("#,##")
        txt_soTien.Select(txt_soTien.Text.Length, 0)
    End Sub

    Private Sub txt_soTien_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_soTien.KeyPress
        If Not Char.IsDigit(e.KeyChar) And Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub btn_chuyenKhoan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_chuyenKhoan.Click
        If txt_soTien.Text = "" Then
            MessageBox.Show("Số tiền chuyển không hợp lệ")
            Return
        End If
        tk1 = cb_tenTK1.SelectedValue.ToString
        tk2 = cb_tenTK2.SelectedValue.ToString
        If tk1 = tk2 Then
            MessageBox.Show("Không thể chuyển tiền trong cùng một TK")
            Return
        End If

        Dim sotien1 As Decimal = Val(dsTK.Rows(cb_tenTK1.SelectedIndex)(2))
        Dim sotien2 As Decimal = Val(dsTK.Rows(cb_tenTK2.SelectedIndex)(2))
        Dim sotienchuyen As Decimal = txt_soTien.Text

        If sotien1 < sotienchuyen Then
            MessageBox.Show("Tài khoản 1 không đủ tiền!")
            Return
        End If

        sotien1 = sotien1 - sotienchuyen
        sotien2 = sotien2 + sotienchuyen

        Dim truyvan As String

        truyvan = "Update [dbo].[NganHang] Set SoTien=" & sotien1.ToString & " Where MaNganHang ='" & tk1 & "'"
        CSDL.Thuc_hien_lenh(truyvan)
        truyvan = "Update [dbo].[NganHang] Set SoTien=" & sotien2.ToString & " Where MaNganHang ='" & tk2 & "'"
        CSDL.Thuc_hien_lenh(truyvan)
        MessageBox.Show("Chuyển khoản thành công!")

        truyvan = "Select MaNganHang,Ten, SoTien From [dbo].[NganHang] where TinhTrang=0 and MaUser=" & My.Settings.MaUser.ToString
        dsTK = CSDL.Doc(truyvan)
        SetupChuyenKhoan()

    End Sub
End Class