﻿Public Class Form_MainScreen

    Private Sub QuảnLýngườiDùngToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim child As New Form_QuanLyNguoiDung
        child.MdiParent = Me
        child.Show()
    End Sub

    Private Sub TàiKhoảnNgânHàngToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TàiKhoảnNgânHàngToolStripMenuItem.Click
        Dim child As New Form_QuanLyTaiKhoan
        child.MdiParent = Me
        child.Show()

    End Sub

    Sub login()

        If My.Settings.Status = True And My.Settings.MaUser <> "" Then

            If My.Settings.QuyenHan = 0 Then

                AdminToolStripMenuItem.Visible = True
            Else
                AdminToolStripMenuItem.Visible = False

            End If

        End If

    End Sub
    Private Sub Form_MainScreen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        My.Settings.MaUser = ""
        My.Settings.Status = False
        My.Settings.QuyenHan = 1
        My.Settings.Username = ""
        My.Settings.Save()

        Me.Hide()
        Dim login As New Form_Login()
        login.Show()

    End Sub

    Private Sub NewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewToolStripMenuItem.Click
        Application.Restart()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub ĐổiMậtKhẩuToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ĐổiMậtKhẩuToolStripMenuItem.Click
        Dim child As New Form_ThayDoiMatKhau
        child.MdiParent = Me
        child.Show()
    End Sub

    Private Sub ThemNguoiDungToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ThemNguoiDungToolStripMenuItem.Click
        Dim child As New Form_QuanLyNguoiDung
        child.MdiParent = Me
        child.Show()
    End Sub

    Private Sub ChuyểnKhoảnToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChuyểnKhoảnToolStripMenuItem.Click
        Dim child As New Form_ChuyenKhoan
        child.MdiParent = Me
        child.Show()
    End Sub

    Private Sub LoạiChiPhíToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoạiChiPhíToolStripMenuItem.Click
        Dim child As New Form_ChiPhi
        child.MdiParent = Me
        child.Show()
    End Sub

    Private Sub PhiếuChiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PhiếuChiToolStripMenuItem.Click
        Dim child As New Form_PhieuChi
        child.MdiParent = Me
        child.Show()
    End Sub
End Class
