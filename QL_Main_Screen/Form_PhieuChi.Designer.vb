﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_PhieuChi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dt_ngaythang = New System.Windows.Forms.DateTimePicker()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.cb_hienthi = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btn_taikhoan = New System.Windows.Forms.Button()
        Me.btn_loaithu = New System.Windows.Forms.Button()
        Me.lb_sotien = New System.Windows.Forms.Label()
        Me.cb_nguoichi = New System.Windows.Forms.ComboBox()
        Me.cb_taikhoan = New System.Windows.Forms.ComboBox()
        Me.cb_loaichi = New System.Windows.Forms.ComboBox()
        Me.txt_noidung = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_sotien = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txt_ngay_filter = New System.Windows.Forms.MaskedTextBox()
        Me.btn_timkiem = New System.Windows.Forms.Button()
        Me.cb_taikhoan_filter = New System.Windows.Forms.ComboBox()
        Me.cb_nguoichi_filter = New System.Windows.Forms.ComboBox()
        Me.cb_loaichi_filter = New System.Windows.Forms.ComboBox()
        Me.txt_sotien2_filter = New System.Windows.Forms.TextBox()
        Me.txt_sotien1_filter = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_noidung_filter = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btn_thoat = New System.Windows.Forms.Button()
        Me.btn_sua = New System.Windows.Forms.Button()
        Me.btn_xoa = New System.Windows.Forms.Button()
        Me.btn_them = New System.Windows.Forms.Button()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Panel1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Khaki
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Arial", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.OrangeRed
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(1008, 49)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "PHIẾU CHI"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.Panel1.Controls.Add(Me.dt_ngaythang)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.btn_taikhoan)
        Me.Panel1.Controls.Add(Me.btn_loaithu)
        Me.Panel1.Controls.Add(Me.lb_sotien)
        Me.Panel1.Controls.Add(Me.cb_nguoichi)
        Me.Panel1.Controls.Add(Me.cb_taikhoan)
        Me.Panel1.Controls.Add(Me.cb_loaichi)
        Me.Panel1.Controls.Add(Me.txt_noidung)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txt_sotien)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(897, 204)
        Me.Panel1.TabIndex = 0
        '
        'dt_ngaythang
        '
        Me.dt_ngaythang.CustomFormat = "dd/MM/yyyy"
        Me.dt_ngaythang.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dt_ngaythang.Location = New System.Drawing.Point(94, 25)
        Me.dt_ngaythang.Name = "dt_ngaythang"
        Me.dt_ngaythang.Size = New System.Drawing.Size(187, 20)
        Me.dt_ngaythang.TabIndex = 0
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.PictureBox1)
        Me.Panel5.Controls.Add(Me.cb_hienthi)
        Me.Panel5.Controls.Add(Me.Label8)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel5.Location = New System.Drawing.Point(697, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(200, 204)
        Me.Panel5.TabIndex = 9
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global._1271029.My.Resources.Resources.Import_Data_Yellow
        Me.PictureBox1.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(194, 168)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'cb_hienthi
        '
        Me.cb_hienthi.FormattingEnabled = True
        Me.cb_hienthi.Items.AddRange(New Object() {"Tháng này", "Tất cả"})
        Me.cb_hienthi.Location = New System.Drawing.Point(67, 177)
        Me.cb_hienthi.Name = "cb_hienthi"
        Me.cb_hienthi.Size = New System.Drawing.Size(121, 21)
        Me.cb_hienthi.TabIndex = 7
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(18, 180)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(46, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "Hiển thị:"
        '
        'btn_taikhoan
        '
        Me.btn_taikhoan.Location = New System.Drawing.Point(625, 24)
        Me.btn_taikhoan.Name = "btn_taikhoan"
        Me.btn_taikhoan.Size = New System.Drawing.Size(24, 23)
        Me.btn_taikhoan.TabIndex = 6
        Me.btn_taikhoan.Text = "+"
        Me.btn_taikhoan.UseVisualStyleBackColor = True
        '
        'btn_loaithu
        '
        Me.btn_loaithu.Location = New System.Drawing.Point(287, 96)
        Me.btn_loaithu.Name = "btn_loaithu"
        Me.btn_loaithu.Size = New System.Drawing.Size(24, 23)
        Me.btn_loaithu.TabIndex = 6
        Me.btn_loaithu.Text = "+"
        Me.btn_loaithu.UseVisualStyleBackColor = True
        '
        'lb_sotien
        '
        Me.lb_sotien.AutoSize = True
        Me.lb_sotien.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lb_sotien.Location = New System.Drawing.Point(456, 63)
        Me.lb_sotien.Name = "lb_sotien"
        Me.lb_sotien.Size = New System.Drawing.Size(45, 15)
        Me.lb_sotien.TabIndex = 4
        Me.lb_sotien.Text = "Label8"
        '
        'cb_nguoichi
        '
        Me.cb_nguoichi.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_nguoichi.FormattingEnabled = True
        Me.cb_nguoichi.Location = New System.Drawing.Point(433, 96)
        Me.cb_nguoichi.Name = "cb_nguoichi"
        Me.cb_nguoichi.Size = New System.Drawing.Size(186, 23)
        Me.cb_nguoichi.TabIndex = 4
        '
        'cb_taikhoan
        '
        Me.cb_taikhoan.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_taikhoan.FormattingEnabled = True
        Me.cb_taikhoan.Location = New System.Drawing.Point(433, 24)
        Me.cb_taikhoan.Name = "cb_taikhoan"
        Me.cb_taikhoan.Size = New System.Drawing.Size(186, 23)
        Me.cb_taikhoan.TabIndex = 1
        '
        'cb_loaichi
        '
        Me.cb_loaichi.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_loaichi.FormattingEnabled = True
        Me.cb_loaichi.Location = New System.Drawing.Point(95, 96)
        Me.cb_loaichi.Name = "cb_loaichi"
        Me.cb_loaichi.Size = New System.Drawing.Size(186, 23)
        Me.cb_loaichi.TabIndex = 3
        '
        'txt_noidung
        '
        Me.txt_noidung.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_noidung.Location = New System.Drawing.Point(95, 134)
        Me.txt_noidung.Multiline = True
        Me.txt_noidung.Name = "txt_noidung"
        Me.txt_noidung.Size = New System.Drawing.Size(524, 64)
        Me.txt_noidung.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(369, 63)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 15)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Số tiền hiện có:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(369, 99)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(62, 15)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Người chi:"
        '
        'txt_sotien
        '
        Me.txt_sotien.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_sotien.Location = New System.Drawing.Point(94, 60)
        Me.txt_sotien.Name = "txt_sotien"
        Me.txt_sotien.Size = New System.Drawing.Size(187, 21)
        Me.txt_sotien.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(369, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 15)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Tài khoản:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(33, 137)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 15)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Nội dung:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(34, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 15)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Loại chi:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(33, 63)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Số tiền:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(34, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Ngày:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.DataGridView1)
        Me.Panel2.Controls.Add(Me.Panel4)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 204)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(897, 408)
        Me.Panel2.TabIndex = 2
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 62)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(897, 346)
        Me.DataGridView1.TabIndex = 1
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.txt_ngay_filter)
        Me.Panel4.Controls.Add(Me.btn_timkiem)
        Me.Panel4.Controls.Add(Me.cb_taikhoan_filter)
        Me.Panel4.Controls.Add(Me.cb_nguoichi_filter)
        Me.Panel4.Controls.Add(Me.cb_loaichi_filter)
        Me.Panel4.Controls.Add(Me.txt_sotien2_filter)
        Me.Panel4.Controls.Add(Me.txt_sotien1_filter)
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Controls.Add(Me.txt_noidung_filter)
        Me.Panel4.Controls.Add(Me.Label14)
        Me.Panel4.Controls.Add(Me.Label10)
        Me.Panel4.Controls.Add(Me.Label11)
        Me.Panel4.Controls.Add(Me.Label13)
        Me.Panel4.Controls.Add(Me.Label15)
        Me.Panel4.Controls.Add(Me.Label12)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(897, 62)
        Me.Panel4.TabIndex = 0
        '
        'txt_ngay_filter
        '
        Me.txt_ngay_filter.Location = New System.Drawing.Point(95, 4)
        Me.txt_ngay_filter.Mask = "00/00/0000"
        Me.txt_ngay_filter.Name = "txt_ngay_filter"
        Me.txt_ngay_filter.Size = New System.Drawing.Size(111, 20)
        Me.txt_ngay_filter.TabIndex = 0
        Me.txt_ngay_filter.ValidatingType = GetType(Date)
        '
        'btn_timkiem
        '
        Me.btn_timkiem.Location = New System.Drawing.Point(810, 6)
        Me.btn_timkiem.Name = "btn_timkiem"
        Me.btn_timkiem.Size = New System.Drawing.Size(75, 48)
        Me.btn_timkiem.TabIndex = 8
        Me.btn_timkiem.Text = "Tìm kiếm"
        Me.btn_timkiem.UseVisualStyleBackColor = True
        '
        'cb_taikhoan_filter
        '
        Me.cb_taikhoan_filter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_taikhoan_filter.FormattingEnabled = True
        Me.cb_taikhoan_filter.Location = New System.Drawing.Point(296, 32)
        Me.cb_taikhoan_filter.Name = "cb_taikhoan_filter"
        Me.cb_taikhoan_filter.Size = New System.Drawing.Size(112, 23)
        Me.cb_taikhoan_filter.TabIndex = 6
        '
        'cb_nguoichi_filter
        '
        Me.cb_nguoichi_filter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_nguoichi_filter.FormattingEnabled = True
        Me.cb_nguoichi_filter.Location = New System.Drawing.Point(296, 3)
        Me.cb_nguoichi_filter.Name = "cb_nguoichi_filter"
        Me.cb_nguoichi_filter.Size = New System.Drawing.Size(112, 23)
        Me.cb_nguoichi_filter.TabIndex = 1
        '
        'cb_loaichi_filter
        '
        Me.cb_loaichi_filter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cb_loaichi_filter.FormattingEnabled = True
        Me.cb_loaichi_filter.Location = New System.Drawing.Point(94, 31)
        Me.cb_loaichi_filter.Name = "cb_loaichi_filter"
        Me.cb_loaichi_filter.Size = New System.Drawing.Size(112, 23)
        Me.cb_loaichi_filter.TabIndex = 5
        '
        'txt_sotien2_filter
        '
        Me.txt_sotien2_filter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_sotien2_filter.Location = New System.Drawing.Point(640, 4)
        Me.txt_sotien2_filter.Name = "txt_sotien2_filter"
        Me.txt_sotien2_filter.Size = New System.Drawing.Size(107, 21)
        Me.txt_sotien2_filter.TabIndex = 4
        '
        'txt_sotien1_filter
        '
        Me.txt_sotien1_filter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_sotien1_filter.Location = New System.Drawing.Point(494, 4)
        Me.txt_sotien1_filter.Name = "txt_sotien1_filter"
        Me.txt_sotien1_filter.Size = New System.Drawing.Size(107, 21)
        Me.txt_sotien1_filter.TabIndex = 2
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(235, 35)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(61, 15)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Tài khoản"
        '
        'txt_noidung_filter
        '
        Me.txt_noidung_filter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_noidung_filter.Location = New System.Drawing.Point(494, 33)
        Me.txt_noidung_filter.Name = "txt_noidung_filter"
        Me.txt_noidung_filter.Size = New System.Drawing.Size(253, 21)
        Me.txt_noidung_filter.TabIndex = 7
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(235, 6)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(62, 15)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Người chi:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(34, 6)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(38, 15)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Ngày:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(33, 35)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 15)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Loại chi:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(607, 7)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(28, 15)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "đến"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(431, 36)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(60, 15)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Nội dung:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(431, 7)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(48, 15)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Số tiền:"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btn_thoat)
        Me.Panel3.Controls.Add(Me.btn_sua)
        Me.Panel3.Controls.Add(Me.btn_xoa)
        Me.Panel3.Controls.Add(Me.btn_them)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(107, 612)
        Me.Panel3.TabIndex = 0
        '
        'btn_thoat
        '
        Me.btn_thoat.Location = New System.Drawing.Point(12, 524)
        Me.btn_thoat.Name = "btn_thoat"
        Me.btn_thoat.Size = New System.Drawing.Size(81, 76)
        Me.btn_thoat.TabIndex = 0
        Me.btn_thoat.Text = "Thoát"
        Me.btn_thoat.UseVisualStyleBackColor = True
        '
        'btn_sua
        '
        Me.btn_sua.Location = New System.Drawing.Point(12, 227)
        Me.btn_sua.Name = "btn_sua"
        Me.btn_sua.Size = New System.Drawing.Size(81, 76)
        Me.btn_sua.TabIndex = 2
        Me.btn_sua.Text = "Sửa"
        Me.btn_sua.UseVisualStyleBackColor = True
        '
        'btn_xoa
        '
        Me.btn_xoa.Location = New System.Drawing.Point(12, 128)
        Me.btn_xoa.Name = "btn_xoa"
        Me.btn_xoa.Size = New System.Drawing.Size(81, 76)
        Me.btn_xoa.TabIndex = 1
        Me.btn_xoa.Text = "Xóa"
        Me.btn_xoa.UseVisualStyleBackColor = True
        '
        'btn_them
        '
        Me.btn_them.Location = New System.Drawing.Point(12, 24)
        Me.btn_them.Name = "btn_them"
        Me.btn_them.Size = New System.Drawing.Size(81, 76)
        Me.btn_them.TabIndex = 0
        Me.btn_them.Text = "Thêm"
        Me.btn_them.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 49)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel3)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Panel2)
        Me.SplitContainer1.Size = New System.Drawing.Size(1008, 612)
        Me.SplitContainer1.SplitterDistance = 107
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'Form_PhieuChi
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(1008, 661)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form_PhieuChi"
        Me.Text = "QUẢN LÝ PHIẾU CHI"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lb_sotien As System.Windows.Forms.Label
    Friend WithEvents cb_nguoichi As System.Windows.Forms.ComboBox
    Friend WithEvents cb_taikhoan As System.Windows.Forms.ComboBox
    Friend WithEvents cb_loaichi As System.Windows.Forms.ComboBox
    Friend WithEvents txt_noidung As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_sotien As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents btn_taikhoan As System.Windows.Forms.Button
    Friend WithEvents btn_loaithu As System.Windows.Forms.Button
    Friend WithEvents btn_them As System.Windows.Forms.Button
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents btn_thoat As System.Windows.Forms.Button
    Friend WithEvents btn_sua As System.Windows.Forms.Button
    Friend WithEvents btn_xoa As System.Windows.Forms.Button
    Friend WithEvents btn_timkiem As System.Windows.Forms.Button
    Friend WithEvents cb_loaichi_filter As System.Windows.Forms.ComboBox
    Friend WithEvents txt_sotien1_filter As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cb_hienthi As System.Windows.Forms.ComboBox
    Friend WithEvents txt_noidung_filter As System.Windows.Forms.TextBox
    Friend WithEvents cb_taikhoan_filter As System.Windows.Forms.ComboBox
    Friend WithEvents cb_nguoichi_filter As System.Windows.Forms.ComboBox
    Friend WithEvents txt_sotien2_filter As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_ngay_filter As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents dt_ngaythang As System.Windows.Forms.DateTimePicker
End Class
