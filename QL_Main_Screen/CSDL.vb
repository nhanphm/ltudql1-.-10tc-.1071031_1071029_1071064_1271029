﻿Imports System.Data.SqlClient

Module CSDL
    'Data Source=.\SQLEXPRESS;AttachDbFilename="E:\Data-study\LAP TRINH UD QL1\DO AN\bitbucket.[ltudql1].[10tc]\ltudql1-.-10tc-.1071031_1071029_1071064_1271029\QL_Main_Screen\bin\Debug\CSDL\QuanLyThuChi.mdf";Integrated Security=True;Connect Timeout=30;User Instance=True

    Public Chuoi_ket_noi As String = "Data Source=.\SQLEXPRESS;AttachDbFilename='" + Application.StartupPath + "\CSDL\QuanLyThuChi.mdf';Integrated Security=True;Connect Timeout=30;User Instance=True"

    Public Function Doc(ByVal Chuoi_lenh As String) As DataTable
        Dim Kq As New DataTable
        ' Khai báo bộ thích ứng 
        Try

            Dim Bo_thich_ung As New SqlDataAdapter(Chuoi_lenh, Chuoi_ket_noi)
            ' Dùng Bộ thích ứng đọc dữ liệu 
            Bo_thich_ung.FillSchema(Kq, SchemaType.Source)
            Bo_thich_ung.Fill(Kq)
        Catch Loi As Exception
            Dim Thong_bao_loi As String = "Lỗi truy xuất CSDL " & vbCrLf
            Thong_bao_loi &= "Chuỗi lệnh :" & Chuoi_lenh & vbCrLf
            Thong_bao_loi &= "Chuỗi kết nối :" & Chuoi_ket_noi & vbCrLf
            Thong_bao_loi &= "Lỗi :" & Loi.Message & vbCrLf
            MessageBox.Show(Thong_bao_loi)
        End Try
        Return Kq
    End Function


    Public Function Doc_cau_truc(ByVal Ten_bang As String) As DataTable
        Dim Kq As New DataTable
        Dim Chuoi_lenh As String = "Select * From " & Ten_bang
        Try
            ' Khai báo bộ thích ứng 
            Dim Bo_thich_ung As New SqlDataAdapter(Chuoi_lenh, Chuoi_ket_noi)
            ' Dùng Bộ thích ứng đọc dữ liệu 
            Bo_thich_ung.FillSchema(Kq, SchemaType.Source)
            Bo_thich_ung.Fill(Kq)
        Catch Loi As Exception
            Dim Thong_bao_loi As String = "Lỗi truy xuất CSDL " & vbCrLf
            Thong_bao_loi &= "Chuỗi lệnh :" & Chuoi_lenh & vbCrLf
            Thong_bao_loi &= "Chuỗi kết nối :" & Chuoi_ket_noi & vbCrLf
            Thong_bao_loi &= "Lỗi :" & Loi.Message & vbCrLf
            MessageBox.Show(Thong_bao_loi)
        End Try

        Return Kq
    End Function

    Public Function Ghi(ByVal Bang As DataTable, ByVal Ten_bang As String) As Integer
        Dim Kq As Integer = 0
        Dim Chuoi_lenh As String = "Select * From " & Ten_bang
        Try
            Dim Bo_thich_ung As New SqlDataAdapter(Chuoi_lenh, Chuoi_ket_noi)
            Dim Bo_phat_sinh_lenh As New SqlCommandBuilder(Bo_thich_ung)
            'AddHandler Bo_thich_ung.RowUpdated, AddressOf Cap_nhat_ma_so
            Kq = Bo_thich_ung.Update(Bang)
        Catch Loi As Exception
            Dim Thong_bao_loi As String = "Lỗi truy xuất CSDL " & vbCrLf
            Thong_bao_loi &= "Chuỗi lệnh :" & Chuoi_lenh & vbCrLf
            Thong_bao_loi &= "Chuỗi kết nối :" & Chuoi_ket_noi & vbCrLf
            Thong_bao_loi &= "Lỗi :" & Loi.Message & vbCrLf
            MessageBox.Show(Thong_bao_loi)
        End Try
        Return Kq
    End Function

    Public Function Thuc_hien_lenh(ByVal Chuoi_lenh As String) As Integer
        Dim Kq As Integer
        ' Khai báo bộ thực hiện lệnh 
        Dim Ket_noi As New SqlConnection(Chuoi_ket_noi)
        Ket_noi.Open()
        Dim Bo_thuc_hien_lenh As New SqlCommand(Chuoi_lenh, Ket_noi)
        ' Sử dụng bộ thực hiện lệnh đọc số lượng các đơn vị
        Kq = Bo_thuc_hien_lenh.ExecuteScalar
        Ket_noi.Close()
        Return Kq
    End Function

    Private Sub Cap_nhat_ma_so(ByVal sender As Object, ByVal e As SqlRowUpdatedEventArgs)
        ' Lậy kết nối hiện dùng 
        Dim Ket_noi As SqlConnection = e.Command.Connection
        ' Kiểm tra là lệnh chèn vào dòng mới 
        If e.StatementType = StatementType.Insert Then
            ' Khởi tạo lệnh đọc mã số phát sinh tự động của dòng vừa chèn vào 
            Dim Lenh As New SqlCommand("Select @@IDENTITY", Ket_noi)
            ' Thực hiện lệnh để nhận mã số phát sinh tự động 
            Dim Ma_so As Integer = Integer.Parse(Lenh.ExecuteScalar())
            e.Row(0) = Ma_so
        End If
    End Sub

End Module
